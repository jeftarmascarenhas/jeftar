import React from "react"

import Layout from "../layouts/en"
import { Header, BannerHome, SEO } from "../components"

const headerInfo = {
  siteTitle: "Jeftar Mascarenhas",
  siteDescription:
    "I love working Front-end, Back-end, and with Design for fun",
}

const IndexPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="Home" />
    <Header {...headerInfo} />
    <BannerHome />
  </Layout>
)

export default IndexPage
