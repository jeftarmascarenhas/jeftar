import React from "react"

import Layout from "../layouts/pt"
import { SEO, Header } from "../components"

const headerInfo = {
  siteTitle: "Sobre",
  siteDescription: "Um pouco mais sobre mim",
}

const AboutPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="About" />
    <Header {...headerInfo} />
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </Layout>
)

export default AboutPage
