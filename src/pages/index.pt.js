import React from "react"

import Layout from "../layouts/pt"
import { Header, BannerHome, SEO } from "../components"

const headerInfo = {
  siteTitle: "Jeftar Mascarenhas",
  siteDescription: "Amo Front-end, back-end e faço Design por diversão",
}

const IndexPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="Home" />
    <Header {...headerInfo} />
    <BannerHome />
  </Layout>
)

export default IndexPage
