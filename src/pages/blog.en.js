import React from "react"
import { Link } from "gatsby"

import Layout from "../layouts/en"
import { SEO, Header, Card, CardHeader, CardFooter } from "../components"

const headerInfo = {
  siteTitle: "Blog",
  siteDescription: "Fast articles about development and tecnologies",
}

const SecondPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="Page two" />
    <Header {...headerInfo} />
    <Link to="/">Go back to the homepage</Link>
    <ul
      style={{
        listStyle: "none",
        display: "flex",
        flexWrap: "wrap",
        marginTop: "3em",
      }}
    >
      <li style={{ width: "50%" }}>
        <Card cursor>
          <CardHeader>
            <h2 className="title">Designing New Ways to Give Context</h2>
            <h3 className="resume">
              When scrolling through News Feed, it’s sometimes difficult to
              judge the credibility of an article.
            </h3>
          </CardHeader>
          <CardFooter borderTop>
            <h5 className="category">React</h5>
          </CardFooter>
        </Card>
      </li>
      <li style={{ width: "50%" }}>
        <Card cursor>
          <CardHeader>
            <h2 className="title">Designing New Ways to Give Context</h2>
            <h3 className="resume">
              When scrolling through News Feed, it’s sometimes difficult to
              judge the credibility of an article.
            </h3>
          </CardHeader>
          <CardFooter borderTop>
            <h5 className="category">React</h5>
          </CardFooter>
        </Card>
      </li>
    </ul>
  </Layout>
)

export default SecondPage
