import React from "react"
import { Link } from "gatsby"

import Layout from "../layouts/pt"
import { SEO, Header } from "../components"

const headerInfo = {
  siteTitle: "Blog",
  siteDescription: "Artigos rápido soubre desenvolvimento e tecnologia",
}

const SecondPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="Page two" />
    <Header {...headerInfo} />
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default SecondPage
