import React from "react"

import Layout from "../layouts/en"
import { SEO, Header } from "../components"

const headerInfo = {
  siteTitle: "About",
  siteDescription: "A lettle better about me",
}

const AboutPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="About" />
    <Header {...headerInfo} />
  </Layout>
)

export default AboutPage
