import React from "react"
import styled from "styled-components"
import theme from "styled-theming"
import PropTypes from "prop-types"

const HeaderWrapper = styled.header`
  position: relative;
  z-index: 2;
  top: 1.2em;
`

const themeColor = theme("mode", {
  light: "#ffda00",
  dark: "#fff",
})

const HeaderTitle = styled.h1`
  color: ${themeColor};
  text-transform: uppercase;
  letter-spacing: 0.15em;
`

const HeaderDescription = styled.h2`
  color: ${themeColor};
  font-size: 1.33em;
  font-weight: 300;
`

const Header = ({ siteTitle, siteDescription }) => (
  <HeaderWrapper>
    <div>
      <HeaderTitle style={{ margin: 0 }}>{siteTitle}</HeaderTitle>
      <HeaderDescription>{siteDescription}</HeaderDescription>
    </div>
  </HeaderWrapper>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
  siteDescription: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
  siteDescription: ``,
}

export default Header
