// import React from "react"
// import PropTypes from "prop-types"
import styled from "styled-components"
import theme from "styled-theming"

const backgroundTheme = theme("mode", {
  light: "#fff",
  dark: "#222",
})

const colorTheme = theme("mode", {
  light: "#222",
  dark: "#fff",
})

const Card = styled.div`
  background-color: ${backgroundTheme};
  position: relative;
  box-shadow: 0 20px 20px rgba(0, 0, 0, 0.08);
  transition: all 250ms cubic-bezier(0.02, 0.01, 0.47, 1);
  &:hover {
    transform: translate(0, -20px);
    box-shadow: 0 40px 40px rgba(0, 0, 0, 0.16);
  }
  margin-bottom: 0.8em;
  cursor: ${props => (props.cursor ? "pointer" : "auto")};
  height: 480px;
`
const CardHeader = styled.div`
  padding: 1em 0.8em;
  .title {
    font-size: 1.1em;
    color: ${colorTheme};
  }
  .resume {
    font-size: 0.8em;
    font-weight: 300;
    color: #c6c6c6;
  }
  @media (min-width: 968px) {
    .title {
      font-size: 1.4em;
    }
  }
`
const CardBody = styled.div`
  padding: 0.8em;
`
const CardImage = styled.div``
const CardFooter = styled.div`
  border-top: ${props => (props.borderTop ? "thin solid #3C3C3C" : "none")};
  padding: 0.8em;
  margin-left: 0.8em;
  margin-right: 0.8em;
  .category {
    font-size: 0.85em;
    color: ${colorTheme};
  }
`

export { Card, CardHeader, CardBody, CardImage, CardFooter }
