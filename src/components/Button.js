import styled from "styled-components"
import theme from "styled-theming"
import PropTypes from "prop-types"

const backgrounColor = theme.variants("mode", "variant", {
  default: { light: "#222", dark: "#6E6E6E" },
  primary: { light: "blue", dark: "darkblue" },
  success: { light: "green", dark: "darkgreen" },
  warning: { light: "orange", dark: "darkorange" },
})

const Button = styled.button`
  background-color: ${backgrounColor};
  border: none;
  border-radius: ${props => (props.around ? "100%" : "0.4rme")};
`

Button.defaultProps = {
  variant: "default",
  around: false,
}

Button.propTypes = {
  variant: PropTypes.oneOf(["default", "primary", "success", "warning"]),
  around: PropTypes.bool,
}

export default Button
