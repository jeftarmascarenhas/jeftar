import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import theme from "styled-theming"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faGithub,
  faTwitter,
  faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons"

const themeColor = theme("mode", {
  light: "#ffda00",
  dark: "#fff",
})

const SidebarWrapper = styled.aside`
  display: flex;
  padding: 1em;
  margin-top: 1.2em;
  @media (min-width: 968px) {
    width: 110px;
    flex-direction: column;
    align-items: center;
  }
`

const Ul = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`

const NavLink = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #6e6e6e;
  border-radius: 100%;
  padding: 0.2rem;
  width: 28px;
  height: 28px;
  @media (min-width: 968px) {
    width: 46px;
    height: 46px;
  }
`

const IntLink = styled(Link)`
  display: block;
  text-decoration: none;
  text-transform: uppercase;
  font-size: 0.8em;
  color: ${themeColor};
  &:first-child {
    border-bottom: 1px solid #6e6e6e;
  }
  .active {
    color: red;
  }
`

const Footer = styled.footer`
  .copyright {
    font-size: 0.8em;
  }
  @media (min-width: 968px) {
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1;
    align-self: center;
    .copyright {
      writing-mode: vertical-rl;
      margin-top: 3em;
    }
  }
`

function SidebarRight({ langs }) {
  const links = langs.map(lang => (
    <IntLink
      activeClassName="active"
      selected={lang.selected}
      key={lang.langKey}
      to={lang.link}
    >
      {lang.langKey}
    </IntLink>
  ))
  return (
    <SidebarWrapper>
      <div>
        <Ul>
          <li>
            <NavLink
              href="https://www.linkedin.com/in/jeftarmascarenhas/"
              target="_blank"
            >
              <FontAwesomeIcon color="#fff" icon={faLinkedinIn} />
            </NavLink>
          </li>
          <li>
            <NavLink
              href="https://github.com/jeftarmascarenhas"
              target="_blank"
            >
              <FontAwesomeIcon color="#fff" icon={faGithub} />
            </NavLink>
          </li>
          <li>
            <NavLink href="https://twitter.com/jeftar" target="_blank">
              <FontAwesomeIcon color="#fff" icon={faTwitter} />
            </NavLink>
          </li>
        </Ul>
      </div>
      <div>{links}</div>
      <Footer>
        <span className="copyright">
          Copyright © {new Date().getFullYear()}
        </span>
      </Footer>
    </SidebarWrapper>
  )
}

export default SidebarRight
