import React from "react"
import { Link } from "gatsby"
import { FormattedMessage } from "react-intl"
import styled from "styled-components"
import theme from "styled-theming"

import { Li } from "./SidebarCommon"

const themeColor = theme("mode", {
  light: "#222",
  dark: "#fff",
})

const LinkTheme = styled(Link)`
  span {
    color: ${themeColor};
  }
`

function Navbar() {
  return (
    <ul>
      <Li>
        <LinkTheme to="/">
          <FormattedMessage id="home" />
        </LinkTheme>
      </Li>
      <Li>
        <LinkTheme to="/">
          <FormattedMessage id="talks" />
        </LinkTheme>
      </Li>
      <Li>
        <LinkTheme to="/blog">
          <FormattedMessage id="blog" />
        </LinkTheme>
      </Li>
      <Li>
        <LinkTheme to="/about">
          <FormattedMessage id="about" />
        </LinkTheme>
      </Li>
      <Li>
        <LinkTheme to="/">
          <FormattedMessage id="mentor" />
        </LinkTheme>
      </Li>
    </ul>
  )
}

export default Navbar
