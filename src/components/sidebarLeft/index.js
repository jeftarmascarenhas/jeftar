import React from "react"
import PropTypes from "prop-types"

import logo from "../../images/logo.png"
import { Brand, SidebarWrapper } from "./SidebarCommon"

function SidebarLeft({ Menu }) {
  return (
    <SidebarWrapper>
      <h1>
        <Brand src={logo} alt="Jeftar Mascarenhas" />
      </h1>
      <ul>
        <Menu />
      </ul>
    </SidebarWrapper>
  )
}

SidebarLeft.propTypes = {
  Menu: PropTypes.node.isRequired,
}

export default SidebarLeft
