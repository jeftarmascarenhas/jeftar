import styled from "styled-components"

const Brand = styled.img`
  width: 80px;
  height: 80px;
`

const Li = styled.li`
  a {
    color: #fff;
    text-decoration: none;
  }
`

const SidebarWrapper = styled.nav`
  padding: 1em;
  padding-top: 1.2em;
  display: flex;
  align-items: center;
  right: auto;
  overflow-y: auto;
  @media (min-width: 968px) {
    flex-direction: column;
    width: 210px;
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    z-index: 1;
  }
  ul {
    list-style: none;
    margin: 0;
    display: flex;
    flex-direction: column;
  }
`

export { Brand, Li, SidebarWrapper }
