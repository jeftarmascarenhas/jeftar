import React from "react"
import styled from "styled-components"

import Image from "./image"

const BannerWrapper = styled.div`
  max-width: 800;
  position: relative;
  @media (min-width: 968px) {
    top: -90px;
    left: 30px;
  }
`

const Banner = () => (
  <BannerWrapper>
    <Image src="banner-one.png" />
  </BannerWrapper>
)

export default Banner
