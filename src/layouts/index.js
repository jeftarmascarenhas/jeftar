/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { useState } from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import { getCurrentLangKey, getLangs, getUrlForLang } from "ptz-i18n"
import { IntlProvider } from "react-intl"
import styled, { ThemeProvider, createGlobalStyle } from "styled-components"
import "intl"

import { SidebarLeft, SidebarRight } from "../components"
import "./layout.css"

const GlobalStyle = createGlobalStyle`
 body {
  background-color: ${props => (props.darkColor ? "#222" : "#fff")};
  color: ${props => (props.darkColor ? "#fff" : "#222")};
 }
 h1, h2 ,h3, h4, h5, h6, p, a, span {
  color: ${props => (props.darkColor ? "#fff" : "#222")};
 }
`

const Wrapper = styled.div`
  position: relative;
  overflow: auto;
  display: flex;
  flex-direction: column;
  @media (min-width: 968px) {
    flex-direction: row;
    margin-left: 210px;
  }
`
const Main = styled.main`
  flex: 1 1 auto;
  padding: 1rem;
`

function Layout({ children, location, i18nMessages, Menu }) {
  const [themeMode, setThemeMode] = useState(true)

  function handleChangeTheme() {
    setThemeMode(!themeMode)
  }

  return (
    <StaticQuery
      query={graphql`
        query LayoutQuery {
          site {
            siteMetadata {
              languages {
                defaultLangKey
                langs
              }
            }
          }
        }
      `}
      render={data => {
        const url = location.pathname
        const { langs, defaultLangKey } = data.site.siteMetadata.languages
        const langKey = getCurrentLangKey(langs, defaultLangKey, url)
        const homeLink = `/${langKey}`.replace(`/${defaultLangKey}/`, "/")
        const langsMenu = getLangs(
          langs,
          langKey,
          getUrlForLang(homeLink, url)
        ).map(item => ({
          ...item,
          link: item.link.replace(`/${defaultLangKey}/`, "/"),
        }))
        return (
          <IntlProvider locale={langKey} messages={i18nMessages}>
            <>
              <GlobalStyle darkColor={themeMode} />
              <ThemeProvider theme={{ mode: themeMode ? "dark" : "light" }}>
                <Wrapper>
                  <SidebarLeft Menu={Menu} />
                  <Main>{children}</Main>
                  <SidebarRight
                    themeChange={handleChangeTheme}
                    langs={langsMenu}
                  />
                </Wrapper>
              </ThemeProvider>
            </>
          </IntlProvider>
        )
      }}
    />
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  Menu: PropTypes.node.isRequired,
}

export default Layout
