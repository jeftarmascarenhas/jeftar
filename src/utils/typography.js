import Typography from "typography"

// const typography = new Typography({
//   baseFontSize: "18px",
//   baseLineHeight: 1.666,
//   headerFontFamily: ["Roboto", "sans-serif", "Helvetica", "Arial"],
//   headerWeight: 500,
//   bodyFontFamily: ["Roboto", "sans-serif"],
//   bodyWeight: 300,
//   boldWeight: 700,
// })
const typography = new Typography({
  baseFontSize: "18px",
  baseLineHeight: 1.666,
  googleFonts: [
    {
      name: "Roboto",
      styles: ["300", "400", "700"],
    },
  ],
  headerFontFamily: ["Roboto", "sans-serif"],
  bodyFontFamily: ["Roboto", "sans-serif"],
  headerWeight: 400,
  bodyWeight: 300,
  boldWeight: 700,
})

export const { scale, rhythm, options } = typography
export default typography
